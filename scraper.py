from selectorlib import Extractor
import requests
import json
from datetime import datetime
import pandas as pd
import re

# Create an Extractor by reading from the YAML file
e = Extractor.from_yaml_file('search_results.yml')

def scrape(url):  

    headers = {
        'dnt': '1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'sec-fetch-dest': 'document',
        'referer': 'https://www.amazon.com/',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
    }

    # Download the page using requests
    print("Downloading %s"%url)
    r = requests.get(url, headers=headers)
    # Simple check to check if page was blocked (Usually 503)
    if r.status_code > 500:
        if "To discuss automated access to Amazon data please contact" in r.text:
            print("Page %s was blocked by Amazon. Please try using better proxies\n"%url)
        else:
            print("Page %s must have been blocked by Amazon as the status code was %d"%(url,r.status_code))
        return None
    # Pass the HTML of the page and create 
    return e.extract(r.text)

# product_data = []



def format_extracted_data(data):
    formatted_products = []
    
  
    processor_pattern = re.compile(
        r'\b('  # Start of the processor name group
        r'Intel\s*(?:Core\s*)?i[3579]-\d{4,5}[A-Z]?|'
        r'Intel\s*Core\s*i7-11800H|'
        r'Intel\s*Core\s*i7-12650H(?:X)?|'  # Added HX series for Intel
        r'Intel\s*N\d{4}|'
        r'AMD\s*Ryzen™?\s*\d{1,2}\s*\d{4}[A-Z]*|'
        r'AMD\s*A[468]-\d{4}[A-Z]?|'
        r'AMD\s*E[2468]-\d{4}[A-Z]?|'
        r'AMD\s*FX-\d{4}[A-Z]?|'
        r'AMD\s*Ryzen\s*\d{1,2}\s*\d{4}[A-Z]*HX?|'  # Added HX series for AMD
        r'Apple\s*M[12]|'
        r'Intel®\s*Core™\s*\d{2}\s*Gen\s*U\d{3}|'
        r'Intel\s*Celeron\s*N\d{4}|'
        r'Intel\s*Celeron\s*Quad-Core|'
        r'Intel\s*Pentium\s*N\d{4}|'
        r'Celeron\s*N\d{4}|'
        r'Celeron\s*Quad\s*Core|'
        r'MediaTek\s*[0-9]+|'
        r'MediaTek\s*[A-Z]*\s+[A-Z]*|'
        r'Intel\s*Pentium®\s*[A-Z]?[0-9]{1,4}|'
        r'Intel®\s*Pentium®\s*[A-Z]?[0-9]{1,4}|'
        r'Intel\s*Xeon\s*[A-Z]?[0-9]{1,4}|'
        r'Intel\s*Core\s*[iI]5-[0-9A-Za-z]+|'
        r'Intel\s*Core\s*[iI]7-[0-9A-Za-z]+|'
        r'Intel®\s*Celeron®\s*[A-Z]?[0-9]{1,4}|'
        r'Intel®\s*Core™\s*(?:13th|12th|11th|Custom)\s*Gen\s*[Uu][0-9]{3}|'
        r'Intel\s*Core\s*i9-[0-9A-Za-z]+|'
        r'Intel\s*Pentium\s*[A-Z]?[0-9]{1,4}|'  # Intel Pentium N series
        r'Intel\s*Core\s*i3-N\d{3,4}|'  # Intel Core i3-N305 series
        r'Intel\s*Pentium\s*Silver\s*N\d{4}|'  # Intel Pentium Silver N6000 series
        r'Intel\s*Core\s*i5-[0-9A-Za-z]+|'  # Int
        r'Intel\s*Core\s*i9-[0-9A-Za-z]+\s*H[0-9]{2}|'
        r'Intel\s*Core\s*i5-[0-9A-Za-z]+\s*H[0-9]{2}|'
        r'AMD\s*Ryzen™?\s*\d{1,2}\s*\d{4}[A-Z]*[GgXxHhUu]?|'
        r'AMD\s*Ryzen\s*\d{1,2}\s*\d{4}[A-Z]*[GgXxHhUu]?'
        r')\b', re.IGNORECASE
    )


    
    for product in data:
        # Skip extraction if '/sspa' is in the URL
        if '/sspa' in product.get('url', ''):
            continue

        # Extract the processor name from the title
        processor_match = processor_pattern.search(product.get('title', ''))
        processor = processor_match.group() if processor_match else ''

        # Extract the unique identifier (ASIN) from the URL
        query_match = re.search(r'/dp/([A-Z0-9]{10})|/product/([A-Z0-9]{10})|/gp/product/([A-Z0-9]{10})', product.get('url', ''))
        query = ''
        if query_match:
            query = query_match.group(1) or query_match.group(2) or query_match.group(3)

        # Format the rating by taking only the first 3 characters
        rating = str(product.get('rating', ''))[:3]

        formatted_product = {
            "title": product.get('title', '').split()[0] if product.get('title') else '',
            "processor": processor,
            "query": query,
            "rating": rating,
            "reviews_count": product.get('reviews'),
            "price": product.get('price'),
            "search_url": product.get('search_url')
        }
        formatted_products.append(formatted_product)
    return formatted_products




all_extracted_data = []

with open("search_results_urls.txt", 'r') as urllist:
    for url in urllist.read().splitlines():
        extracted_data = scrape(url)
        if extracted_data and 'products' in extracted_data:
            formatted_data = format_extracted_data(extracted_data['products'])
            all_extracted_data.extend(formatted_data)

# Convert to DataFrame
df = pd.DataFrame(all_extracted_data)

# Add a timestamp
timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
filename = f'search_results_{timestamp}.csv'

# Save to CSV
df.to_csv(filename, index=False)
print(f'Data saved to {filename}')